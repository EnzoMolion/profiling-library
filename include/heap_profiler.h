#include <gperftools/heap-profiler.h>

void init_heap_profiling (char const * prefix);

void end_heap_profiling ();

/* 
 * Dump the heap usage in the file starting by "prefix" at the moment of the call of this function
 */
void heapProfilerDump(char const * prefix);