#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#ifdef USE_MPI
#include <mpi.h>
#endif

/* 
 * After execution of this function, allocation sizes will be recorded
 * The first argument is the number of following arguments
 * The arguments given after nb_args must be of type 'long' and represent the size of allocations to record callstack of.
 * Specific values :   0 indicates large sized allocations (>1MiB)
 *                    -1 indicates medium sized allocations (>256KiB & <1MiB)
 *                    -2 indicates small sized allocations (<256KiB)
 * Example : init_size_profiling(3, (long) -1, 192, 4096) will initialize size_profiling and record callstacks
 *           of allocations of size 192B, 4096B and of medium size (>256KiB & <1MiB)
 */
void init_size_profiling(unsigned int nb_args, ...);

/*
 * After execution of this function, allocation sizes will not be recorded anymore
 */
void end_size_profiling();

/*
 * Reset allocation sizes record
 */
void reset_size_profiling();

/*
 * Dumps allocation record(s) into 'file_prefix.{extensions}'
 */
void dump_size_profiling(const char * file_prefix, const char *mode);

/* 
 * Weak symbols
 */
extern "C" void *__libc_malloc(size_t size);
extern "C" void *__libc_realloc(void *ptr, size_t size);
extern "C" void *__libc_calloc(size_t nmemb, size_t size);

/* 
 * Reimplementation (hook) of malloc, calloc & realloc
 */
void* malloc(size_t size) throw ();
void* realloc(void* ptr, size_t size) throw ();
void* calloc (size_t nmemb, size_t size) throw ();
