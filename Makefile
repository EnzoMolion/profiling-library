-include makefile.inc

SRCDIR=src/
INCDIR=-Iinclude/
OBJDIR=build/
LIBDIR=lib/

CPU_DEFS += -DCPU_PROFILING
CPU_LIBS += -lprofiler
CPU_OBJ += $(OBJDIR)cpu_profiler.o
CPU_INCDIR += -I$(TCMALLOC_DIR)

HEAP_DEFS += -DHEAP_PROFILING
HEAP_LIBS += -ltcmalloc
HEAP_OBJ += $(OBJDIR)heap_profiler.o
HEAP_INCDIR += -I$(TCMALLOC_DIR)


SIZE_DEFS += -DSIZE_PROFILING
SIZE_LIBS +=
SIZE_OBJ += $(OBJDIR)size_profiler.o
ifeq ($(USE_MPI), 1)
    SIZE_DEFS += -DUSE_MPI
endif
ifeq ($(RECORD_EXACT_SIZE), 1)
    SIZE_DEFS += -DEXACT_SIZE
endif
ifeq ($(RECORD_EXACT_SIZE_THRESHOLD), 0)
else
    SIZE_DEFS += -DEXACT_SIZE_THRESHOLD=${RECORD_EXACT_SIZE_THRESHOLD}
endif


# CC = g++
CC = mpic++
CFLAGS = -Wall
SHFLAGS = -g -fPIC
LDFLAGS = -ldl

ALL_DEFS += $(CPU_DEFS) $(HEAP_DEFS) $(SIZE_DEFS)
ALL_LIBS += $(CPU_LIBS) $(HEAP_LIBS) $(SIZE_LIBS)

EXECS=main
NEEDED_DIRECTORIES = ./lib/ ./build/
default:directories $(LIBDIR)libprofilingcpu.so $(LIBDIR)libprofilingheap.so $(LIBDIR)libprofilingsize.so

directories:
	@mkdir -p ./lib/ ./build/

./lib/:
	@mkdir -p ./lib/
./build/:
	@mkdir -p ./build/


###############################################################################
### Library
###############################################################################

$(OBJDIR)%.o: $(SRCDIR)%.cpp
	$(CC) $(SIZE_DEFS) -fopenmp $(CXX_FLAGS) $(SHFLAGS) -c $^ -o $@ $(CFLAGS) $(INCDIR) $(LDFLAGS)

$(OBJDIR)cpu_profiler.o: $(SRCDIR)cpu_profiler.cpp
	$(CC) $(SIZE_DEFS) $(CXX_FLAGS) $(SHFLAGS) -c $^ -o $@ $(CFLAGS) $(INCDIR) $(CPU_INCDIR) $(LDFLAGS)

$(OBJDIR)heap_profiler.o: $(SRCDIR)heap_profiler.cpp
	$(CC) $(SIZE_DEFS) $(CXX_FLAGS) $(SHFLAGS) -c $^ -o $@ $(CFLAGS) $(INCDIR) $(HEAP_INCDIR) $(LDFLAGS)

$(LIBDIR)libprofilingcpu.so: $(OBJ) $(CPU_OBJ)
	$(CC) -g -shared -o $@ $^ $(CFLAGS)

$(LIBDIR)libprofilingheap.so: $(OBJ) $(HEAP_OBJ)
	$(CC) -g -shared -o $@ $^ $(CFLAGS)

$(LIBDIR)libprofilingsize.so: $(OBJ) $(SIZE_OBJ)
	$(CC) $(SIZE_DEFS) -g -shared -o $@ $^ $(CFLAGS)


###############################################################################
### Binary
###############################################################################

ifeq ($(USE_TCMALLOC),1)
    MEMFLAGS = -ltcmalloc
endif

ifeq ($(USE_JEMALLOC),1)
    MEMFLAGS = -L`jemalloc-config --libdir` -Wl,-rpath,`jemalloc-config --libdir` -ljemalloc `jemalloc-config --libs`
endif

ifeq ($(USE_CPUPROFILING), 1)
    DEFS += $(CPU_DEFS)
    LIBS += $(CPU_LIBS)
    DYN_LIBS += $(LIBDIR)libprofilingcpu.so
    PROF_LDFLAGS += -lprofilingcpu
    INCDIR += $(CPU_INCDIR)
endif
ifeq ($(USE_HEAPPROFILING), 1)
    DEFS += $(HEAP_DEFS)
    LIBS += $(HEAP_LIBS)
    DYN_LIBS += $(LIBDIR)libprofilingheap.so
    PROF_LDFLAGS += -lprofilingheap
    INCDIR += $(HEAP_INCDIR)
endif
ifeq ($(USE_SIZEPROFILING), 1)
    DEFS += $(SIZE_DEFS)
    LIBS += $(SIZE_LIBS)
    DYN_LIBS += $(LIBDIR)libprofilingsize.so
    PROF_LDFLAGS += -lprofilingsize
endif


main:main.cpp $(NEEDED_DIRECTORIES) $(DYN_LIBS) 
	$(CC) -O0 -g -o $@ $< $(CFLAGS) $(DEFS) $(INCDIR) $(MEMFLAGS) -L./lib -Wl,-rpath,./lib $(PROF_LDFLAGS) $(LIBS) $(LDFLAGS) -fopenmp



###############################################################################
### Cleaning & misc.
###############################################################################
.PHONY: clean
clean:
	rm -f $(OBJDIR)*.o $(LIBDIR)*.so $(EXECS)
