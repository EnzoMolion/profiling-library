#include <iostream>

#ifdef CPU_PROFILING  
#include "cpu_profiler.h"
#endif 
#ifdef HEAP_PROFILING
#include "heap_profiler.h"
#endif
#ifdef SIZE_PROFILING
#include "size_profiler.h"
#endif

void large_alloc(){
  int * dumb_ptr = (int *) malloc(1000000000);
  fprintf(stderr, "Ptr : %p\n", dumb_ptr);
  free(dumb_ptr);
}

void nested_large_alloc(){
  large_alloc();
}


int main () {

  #if defined CPU_PROFILING || defined HEAP_PROFILING
  std::cout << "##### " ;
  #ifdef CPU_PROFILING 
  std::cout << "CPU_PROFILING " ;
  #endif 
  #ifdef HEAP_PROFILING
  std::cout << "HEAP_PROFILING " ;
  #endif
  std::cout << "#####\n" ;
  #endif

  #ifdef HEAP_PROFILING
  init_heap_profiling("./profiling.heap");
  #endif

  #ifdef CPU_PROFILING
  init_cpu_profiling("./profiling.cpu");
  #endif

  #ifdef SIZE_PROFILING
  init_size_profiling(3, 0, (long) -1, (long) -2);
  #endif

  void ** pp_array ;
  int number_of_allocations = 50000 ;
  int size_of_allocations = 4096 ;

  pp_array = (void **) malloc(number_of_allocations * sizeof(void *));
  for (int i = 0; i < number_of_allocations; ++i){
    pp_array[i] = malloc(size_of_allocations);
  }

  for (int i = 0; i < number_of_allocations; ++i){
    free(pp_array[i]);
  }
  free(pp_array);

  free(malloc(2400000));
  free(malloc(2400000));

  #ifdef SIZE_PROFILING
  dump_size_profiling("traces/intermediate", "a");
  reset_size_profiling();
  #endif

  large_alloc();
  nested_large_alloc();

  #if defined CPU_PROFILING || defined HEAP_PROFILING 
  std::cout << "##### /" ;
  #ifdef CPU_PROFILING 
  std::cout << "CPU_PROFILING " ;
  #endif
  #ifdef HEAP_PROFILING
  std::cout << "HEAP_PROFILING " ;
  #endif
  std::cout << "#####\n" ;
  #endif

  #ifdef HEAP_PROFILING
  heapProfilerDump("./profiler.heap");
  end_heap_profiling();
  #endif

  #ifdef CPU_PROFILING
  end_cpu_profiling();
  #endif

  #ifdef SIZE_PROFILING
  dump_size_profiling("traces/dump", "a");
  end_size_profiling();
  #endif

}