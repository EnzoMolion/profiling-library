###############################################################################
### Library
###############################################################################

USE_LIBCMALLOC      := 1
USE_TCMALLOC        := 0
    TCMALLOC_DIR        := /usr/local/include/gperftools/
USE_JEMALLOC        := 0



###############################################################################
### Binary
###############################################################################

# Profile CPU usage (tcmalloc)
USE_CPUPROFILING        := 0

# Profile HEAP usage (tcmalloc)
USE_HEAPPROFILING       := 0

# Record size of each {m,c,re}alloc call
USE_SIZEPROFILING       := 1
    USE_MPI                     := 0
    RECORD_EXACT_SIZE           := 0
    # Ignored if 0, threshold quantity to print 'exact size' number of allocations 
    RECORD_EXACT_SIZE_THRESHOLD := 0
