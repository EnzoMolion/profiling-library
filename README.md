# Profiling
Three librairies that allow user to easily profile a program's :
- CPU usage (heavily based on TCMalloc cpu profiler),
- heap usage (heavily based on TCMalloc heap profiler),
- different sizes of allocation and their corresponding callstacks.

## Usage
To use those libraries, [compile them](#compilation) and then link your executable against generated `.so` file(s).

### Size profiling
* ```void init_size_profiling(unsigned int nb_args, ...);```
    __After execution of this function, allocation sizes will be recorded.__
    The first argument (`nb_args`) is the number of following arguments.
    The arguments given after `nb_args` must be of type `nb_args` and represent the size of allocations to record callstack of.
    Specific values :
    * `0` indicates large sized allocations (>1MiB)
    * `-1` indicates medium sized allocations (>256KiB & <1MiB)
    * `-2` indicates small sized allocations (<256KiB).

    _Example_ : `init_size_profiling(3, (long) -1, 192, 4096)` will initialize size_profiling and record callstacks of allocations of size 192B, 4096B and of medium size (>256KiB & <1MiB)

    Must be run before any other function of size profiling.


* ```void end_size_profiling();```
    __After execution of this function, allocation sizes will not be recorded anymore.__
    No other function than `init_size_profiling` must be run after this function.

* ```void reset_size_profiling();```
    __Resets allocation sizes record.__


* ```void dump_size_profiling(const char * file_prefix, const char *mode);```
    __Dumps allocation record(s) into `file_prefix.{extensions}`.__
    `mode` corresponds to the opening mode of `file_prefix.{extensions}`, hence it should be one among {`"w"`, `"a"`, `"r+"`, `"w+"`, `"a+"`}.

### CPU & Heap profiling
#### CPU profiling
* ```void init_cpu_profiling(char const * prefix);```
    __After execution of this function, CPU usage will be profiled.__
    The result of profiling will be dumped in `prefix.{extension}`.

* ```void end_cpu_profiling();```
    __After execution of this function, CPU usage will not be profiled anymore.__
    The result of profiling will be dumped in `prefix.{extension}` stated in `init_cpu_profiling`.
    This function must be called _after_ `init_cpu_profiling`.



#### Heap profiling
* ```void init_heap_profiling(char const * prefix);```
    __After execution of this function, heap usage will be profiled.__
    Any unsolicited dump will be made into `prefix.{extension}`.
    Must be run before any other function of heap profiling.

* ```void end_heap_profiling();```
    __After execution of this function, heap usage will not be profiled anymore.__
    No other function than `init_heap_profiling` must be run after this function.

* ```void heapProfilerDump(char const * prefix);```
    __Dumps heap profiling into `prefix.{extension}`.__



## Compilation <a name="compilation"></a>
You can compile those libraries by running the command `make`. 

You'll find a trivial example nammed `main.cpp` at the root of the project. To test it, run `make main` (after [setting `USE_MPI` option to `0`](#compilationOptions)).

## Compilation options (`makefile.inc`) <a name="compilationOptions"></a>
To select which option to enable, set their value to `1` and to disable it set it to `0`.

* `USE_SIZEPROFILING` enables recording of size of {m,c,re}alloc calls, only tested with libcmalloc. It might make the application really slow depending on chosen sizes to record and the number of allocations in the profiled application.
    * `USE_MPI` : if your application uses OpenMPI, set this flag to `1`, it will then only record the allocations of processes of rank `0`,
    * `RECORD_EXACT_SIZE` enables the exact size recording of small allocations, meaning that every size of allocation under 256KiB will have the corresponding line in the resulting file : `<size of allocation> : <number of allocations of this size> (<corresponding amount of memory>)`,
    * `RECORD_EXACT_SIZE_THRESHOLD = value` enables (if `value` is non-zero) a threshold quantity to print the number of allocations if the `RECORD_EXACT_SIZE` option is enabled, meaning that every size having under `value` allocations recorded will not be displayed.


* `USE_CPUPROFILING` enables CPU profiling and will use TCMalloc (hence is incompatible with size profiling)
* `USE_HEAPPROFILING`enables heap profiling and will use TCMalloc (hence is incompatible with size profiling)

## Further improvements

### Size profiling

- Bugfixes :
    + Check memory leaks
- Enhancement
    + Write a different file for every `mpi_rank`
    + Reduce the `omp critical` pragma to the minimal needed section
- New features : 
    + Per thread recording
    + Section system
    + {m, c, re}alloc differentiation