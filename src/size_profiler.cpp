#include "size_profiler.h"


// #################### Allocation function address record ####################

#include <execinfo.h>
#include <malloc.h>
#include <dlfcn.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdarg.h>


#define MAX_NB_FUNCTIONS 1000000
#define MAX_NB_STACKS 100000
#define MAX_STACK_SIZE 100
#define MAX_FUNCTION_SIZE 100

int g_stacks_size = 0 ; // the size of the stacks lists
int g_stacks_sizes[MAX_NB_STACKS] = { 0 } ; // the size of each stack of the lists
int g_stacks_quantities[MAX_NB_STACKS] = { 0 } ; // the size of each stack of the lists
char*** g_stacks ; // the addresses stacks
char*** g_symbolic_stacks ; // the symbolic stacks


/*
 * Prints a_stack of size a_stack_size to a_file
 */
void print_stack(FILE * a_file, char ** a_stack, size_t a_stack_size){
    for (long unsigned int i = 0; i < a_stack_size; i++){
        fprintf(a_file, "[%lu]%s\n", i, a_stack[i]);
    }
    fprintf(a_file, "----------\n");
}

/*
 * Prints the symbolic stack to the file
 */
void print_global_stack(FILE * a_file){

    fprintf(a_file, "#####-----------#####\n");
    fprintf(a_file, "Stack record of size %i: \n", g_stacks_size);
    for (int i = 0; i < g_stacks_size; i++){
        fprintf(a_file, "Stack %i/%i : %i\n", i + 1, g_stacks_size, g_stacks_quantities[i]);
        print_stack(a_file, g_symbolic_stacks[i], g_stacks_sizes[i] - 3);
    }
    fprintf(a_file, "#####-----------#####\n");
}

/*
 * Adds the given stack of given size at the end of symbolic stack (at rank g_stacks_size - 1)
 */
// Relies on g_stacks_size : adds in g_stacks_size - 1
void add_symbolic_stack(char ** a_symbol_stack, size_t a_stack_size){

    // Resize first dimension
    g_symbolic_stacks = (char ***) __libc_realloc(g_symbolic_stacks, (g_stacks_size) * sizeof(char **));

    // Allocate space for stack
    g_symbolic_stacks[g_stacks_size - 1] = (char **) __libc_malloc(a_stack_size * sizeof(char *));

    for (unsigned long int i = 0 ; i < a_stack_size ; ++i){
        // Copy each stack line to it's allocated space in g_symbolic_stacks
        g_symbolic_stacks[g_stacks_size - 1][i] = (char *) __libc_malloc((strlen(a_symbol_stack[i]) + 1) * sizeof(char));
        strcpy(g_symbolic_stacks[g_stacks_size - 1][i], a_symbol_stack[i]);
    }
}

/*
 * Returns the result (of size 'result_size') of 'a_instruction' system call into allocated 'result'
 */ 
void get_result_of_system_call(const char * a_instruction, char*** result, int** result_size){
    FILE* fp;
    char line[1024];
    int index = 0 ;

    char ** r_result = NULL ;

    /* Open the command for reading. */
    fp = popen(a_instruction, "r");
    if (fp == NULL) {
        fprintf(stderr, "Failed to run command\n");
        result = NULL ;
    }
    else{
        /* Read the output a line at a time  */
        while (fgets(line, sizeof(line), fp) != NULL) {
            // trim trailing \n (from https://stackoverflow.com/a/28462221/6304206)
            line[strcspn(line, "\n")] = 0 ; 
            // Resize first dimension
            r_result = (char **) __libc_realloc(r_result, (index + 1) * sizeof(char *)) ;
            // Allocate space for string
            r_result[index] = (char *) __libc_malloc((strlen(line) + 1) * sizeof(char));
            // Copy received string into the array
            strcpy(r_result[index], line);
            index++;
        }
        pclose(fp);
        *result_size = (int *) __libc_malloc(sizeof(**result_size));
        **result_size = index ;
        *result = r_result ;
    }
}

/*
 * Record the symbols stack at the end of g_sybolic stack 
 */
void record_symbol_stack(){

    void *raw_stack[MAX_STACK_SIZE];
    size_t stack_size;
    char **symbols_stack;

    stack_size = backtrace (raw_stack, 30);
    symbols_stack = backtrace_symbols (raw_stack, stack_size);

    // The built symbol stack to be added to record
    char **built_symbol_stack = (char **) __libc_malloc(stack_size * sizeof(char*));

    // starting at 3 to avoid recording malloc-hooking 
    for (unsigned int i = 3; i < stack_size; i++){

        // From https://stackoverflow.com/a/4611112/6304206 (method 4)
        // and https://stackoverflow.com/a/7557756/6304206

        /* Retrieve file name : 
         * Find find first occurence of '(' or ' ' in symbols_stack[i] and assume
         * everything before that is the file name. (Don't go beyond 0 though
         * (string terminator)*/
        size_t p = 0;
        while(symbols_stack[i][p] != '(' && symbols_stack[i][p] != ' ' && symbols_stack[i][p] != 0){
            ++p;
        }

        char stack_prefix[4] ;
        strcpy(stack_prefix, "0x7");
        char address_as_hex[30] ;
        sprintf(address_as_hex, "%p", raw_stack[i]);

        Dl_info info;
        int gotInfo = dladdr(raw_stack[i], &info);
        if(gotInfo == 0){
            fprintf(stderr, "Problem retrieving program information for %p:  %s\n", raw_stack[i], dlerror());
        }

        int address_in_stack = 0 ;
        char demangle_instruction[256];

        address_in_stack = strncmp(address_as_hex, stack_prefix, strlen(stack_prefix)) == 0 ; 

        // Choose how to demangle name, based on if address is on stack
        if(address_in_stack && gotInfo){
            if(info.dli_sname){
                sprintf(demangle_instruction, "c++filt %s", info.dli_sname);
            }
        }
        else{
	    ptrdiff_t offset = (unsigned char *)(raw_stack[i])-(unsigned char *)(raw_stack[stack_size-1]);
            sprintf(demangle_instruction,"addr2line -j .text 0x%lx -e %.*s -f -C", offset, (int) p, symbols_stack[i]);
        }

        // Get the result of demangling instruction (value and size)
        char ** demangled_symbolic_name_result = NULL ;
        int * demangled_symbolic_name_result_size = NULL ;
        get_result_of_system_call(demangle_instruction, &demangled_symbolic_name_result, &demangled_symbolic_name_result_size);

        // Symbolic stack line building
        char line[5000] = { 0 } ;
        char buf[2000] = { 0 } ;
        for (int j = 0; demangled_symbolic_name_result_size && j < *demangled_symbolic_name_result_size; j++){
            if(j == *demangled_symbolic_name_result_size - 1){
                if(address_in_stack && gotInfo){
                    sprintf(buf, "%s", demangled_symbolic_name_result[j]);
                    strcat(line, buf);
                }
                else{
                    sprintf(buf, "%s", demangled_symbolic_name_result[j]);
                    strcat(line, buf);
                }
            }
            else{
                if(address_in_stack && gotInfo){
                    sprintf(buf, "%s @ ", demangled_symbolic_name_result[j]);
                    strcat(line, buf);
                }
                else{
                    sprintf(buf, "%s @ ", demangled_symbolic_name_result[j]);
                    strcat(line, buf);
                }
            }
        }
        if(address_in_stack && gotInfo){
            sprintf(buf, " @ %s", info.dli_fname);
            strcat(line, buf);
        }

        built_symbol_stack[i - 3] = (char *) __libc_malloc((strlen(line) + 1) * sizeof(char));
        strcpy(built_symbol_stack[i - 3], line);
        line[0] = 0 ;
    }

    add_symbolic_stack(built_symbol_stack, (size_t) stack_size - 3);
    free(symbols_stack);
}

/*
 * Compares two stacks 
 * Returns 1 if they are different and 1 otherwise
 * Two stacks are equal if all thei elements are equal on from 0-th to x-th element where x is min{a_fst_size; a_snd_size}
 */
int two_stacks_cmp(char ** a_fst_stack, size_t a_fst_size, char ** a_snd_stack, size_t a_snd_size){
    for (unsigned int i = 0; i < a_fst_size && i < a_snd_size; i++){
        if(strcmp(a_fst_stack[i], a_snd_stack[i]) != 0){
            return 0 ;
        }
    }
    return 1 ;
}

/*
 * Tries to find if the given stack of given size exists within g_stacks
 * Return the index of the stack if it was found, -1 otherwise
 */
int find_stack(char ** a_stack, size_t a_stack_size){
    for (int i = 0; i < g_stacks_size; i++){
        if(two_stacks_cmp(a_stack, a_stack_size, g_stacks[i], g_stacks_sizes[i])){
            return i ;
        }
    }
    return -1 ;
}

/*
 * Adds the given stack of given size to g_stacks as a new one
 */
void add_new_stack(char ** a_stack, size_t a_stack_size){
    // Resize first dimension
    g_stacks = (char ***) __libc_realloc(g_stacks, (g_stacks_size + 1) * sizeof(char **));

    // Allocate space for stack
    g_stacks[g_stacks_size] = (char **) __libc_malloc(a_stack_size * sizeof(char *));

    for (unsigned long int i = 0 ; i < a_stack_size ; ++i){
        // Copy each stack line to it's allocated space in g_stacks
        // g_stacks[g_stacks_size][i] <- a_stack[i] ;
        g_stacks[g_stacks_size][i] = (char *) __libc_malloc((strlen(a_stack[i]) + 1) * sizeof(char));
        strcpy(g_stacks[g_stacks_size][i], a_stack[i]);
    }

    g_stacks_sizes[g_stacks_size] = a_stack_size ;
    g_stacks_quantities[g_stacks_size] = 1 ;
    g_stacks_size++;

}

/*
 * Adds the given stack (of given size) to g_stacks (whether by incrementing g_stacks_quantities if applicable or as a new one otherwise)
 * Returns 1 if it existed and quantity was incremented, 0 if it was new
 */
int add_to_stack_record(char ** a_stack, size_t a_stack_size){
    int found_stack = find_stack(a_stack, a_stack_size);
    if (found_stack == -1){
        add_new_stack(a_stack, a_stack_size);
        return 1 ;
    }else{
        g_stacks_quantities[found_stack]++;
        return 0 ;
    }
}

/*
 * Adds the adresses stack to g_stacks
 * Returns 1 if it existed and quantity was incremented, 0 if it was new
 */
int add_stack(){
    void *stack[50];
    size_t stack_size;
    char *addresses[50];

    stack_size = backtrace (stack, 50);

    for (unsigned int i = 0; i < stack_size; i++){
        // Automalloc from https://stackoverflow.com/a/10388547/6304206
        size_t memory_needed = snprintf(NULL, 0, "%p", stack[i]) + 1; //+1 for ending \0
        addresses[i] = (char *) __libc_malloc(memory_needed * sizeof(char));
        sprintf(addresses[i], "%p", stack[i]);
    }
    return add_to_stack_record(addresses, stack_size);
}


// #################### Allocation size record ####################

#define SMALL_OBJECT_INDEX SMALL_OBJECT_SIZE + 1
#define MEDIUM_OBJECT_INDEX SMALL_OBJECT_INDEX + 1 
#define LARGE_OBJECT_INDEX MEDIUM_OBJECT_INDEX + 1 

#define TOTAL_SIZE LARGE_OBJECT_INDEX + 1

#define SMALL_OBJECT_SIZE 262144
#define MEDIUM_OBJECT_SIZE 1048576

unsigned long long g_total_quantity_small = 0 ;
unsigned long long g_total_quantity_medium = 0 ;
unsigned long long g_total_quantity_large = 0 ;

unsigned long long g_sizes[TOTAL_SIZE] = { 0 };
FILE * g_output_file = NULL;

#ifdef USE_MPI
int g_mpi_enabled = 0 ;
int g_mpi_rank;
#endif

long * g_to_be_recorded = NULL ;
unsigned int g_to_be_recorded_size = 0 ;
#pragma omp threadprivate(g_to_be_recorded, g_to_be_recorded_size)

int g_malloc_hook_active = 0 ;
int g_realloc_hook_active = 0 ;
int g_calloc_hook_active = 0 ; 

// Pretty formating of large numbers
// from http://programanddesign.com/cpp/human-readable-file-size-in-c/
char* readable_fs(double size, char *buf) {
    int i = 0;
    const char* units[] = {"B", "kB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"};
    while (size >= 1024) {
        size /= 1024;
        i++;
    }
    sprintf(buf, "%.*f %s", i, size, units[i]);
    return buf;
}
char* readable_nb(double size, char *buf) {
    int i = 0;
    const char* units[] = {"", "e3", "e6", "e9", "e12", "e15", "e18", "e21", "e24"};
    while (size >= 1000) {
        size /= 1000;
        i++;
    }
    sprintf(buf, "%.*f%s", i, size, units[i]);
    return buf;
}


void print_size_profiling(FILE * a_file){
    #ifdef USE_MPI
    if((g_mpi_enabled && g_mpi_rank == 0) || !g_mpi_enabled){
    #endif
        #ifdef EXACT_SIZE
        fprintf(a_file, "# <size> : <number> (<corresponding amount of memory>)\n");
        #endif
        for (unsigned int i = 0; i < SMALL_OBJECT_INDEX; i++){
            #ifdef EXACT_SIZE_THRESHOLD
            if(g_sizes[i] != 0 && g_sizes[i] >= EXACT_SIZE_THRESHOLD){
            #else
            if(g_sizes[i] != 0){
            #endif
                #ifdef EXACT_SIZE
                // Print <size of allocation> : <number of allocations of this size> (<corresponding amount of memory>)
                char d_number[16], d_quantity[16];
                fprintf(a_file, "%u\t: %8s (%s)\n",  i, readable_nb((double) g_sizes[i], d_number), readable_fs((double) (g_sizes[i] * i), d_quantity));
                #endif
            }
        }
        #ifdef EXACT_SIZE
        fprintf(a_file, "\n");
        #endif
        char d_number[16], d_quantity[16];
        fprintf(a_file, "Small\t: %8s (%s)\n", readable_nb((double) g_sizes[SMALL_OBJECT_INDEX], d_number), readable_fs((double) g_total_quantity_small, d_quantity));
        fprintf(a_file, "Medium\t: %8s (%s)\n", readable_nb((double) g_sizes[MEDIUM_OBJECT_INDEX], d_number), readable_fs((double) g_total_quantity_medium, d_quantity));
        fprintf(a_file, "Large\t: %8s (%s)\n", readable_nb((double) g_sizes[LARGE_OBJECT_INDEX], d_number), readable_fs((double) g_total_quantity_large, d_quantity));
    #ifdef USE_MPI
    }
    #endif
}

/*
 * Checks if size is in callstack recorded list
 * Returns 1 if it is, 0 otherwise
 */
int in_recorded_list(long size){
    for (unsigned int i = 0; i < g_to_be_recorded_size; i++){
        if(g_to_be_recorded[i] == size){
            return 1 ;
        }
    }
    return 0 ;
}

/*
 * Checks if size must be callstack recorded
 * Returns 1 if it must, 0 otherwise
 */
int needs_to_be_recorded(long size){

    // Must be recorded if
    // - size is medium and -1 is in recorded list
    // - size is large and 0 is in recorded list
    // - size is small and -2 is in recorded list
    if((size > SMALL_OBJECT_SIZE && size <= MEDIUM_OBJECT_SIZE && in_recorded_list(-1))
        || (size > MEDIUM_OBJECT_SIZE && in_recorded_list(0))
        || (size <= SMALL_OBJECT_SIZE && in_recorded_list(-2))){
            return 1 ;
        }

    // Must be recorded if size is in recorded list
    return in_recorded_list(size);
}


void init_size_profiling(unsigned int a_nb_args, ...){

    va_list args_list;

    g_to_be_recorded = (long int *) __libc_malloc(a_nb_args * sizeof(int));
    g_to_be_recorded_size = a_nb_args ;

    va_start(args_list, a_nb_args);
    for(unsigned int i = 0; i < a_nb_args; i++) {
        g_to_be_recorded[i] = va_arg(args_list, long int); // using a long because 262144 needs 4 bytes to be stored if signed
        if(g_to_be_recorded[i] == 0) fprintf(stderr, "[%s] All large sized allocations will be recorded\n", __FUNCTION__);
        if(g_to_be_recorded[i] == -1) fprintf(stderr, "[%s] All medium sized allocations will be recorded\n", __FUNCTION__);
        if(g_to_be_recorded[i] == -2) fprintf(stderr, "[%s] All small sized allocations will be recorded\n", __FUNCTION__);
    }
    va_end(args_list);

    #ifdef USE_MPI
    g_mpi_enabled = 1 ;
    #endif
    g_malloc_hook_active = 1 ;
    g_realloc_hook_active = 1 ;
    g_calloc_hook_active = 1 ; 
}


void reset_size_profiling(){
    #ifdef USE_MPI
    if(g_mpi_rank == 0){
    #endif
        for (int i = 0; i < g_stacks_size; i++){
            g_stacks[i] = 0 ;
            g_symbolic_stacks[i] = 0 ;
        }
        for (int i = 0; i < TOTAL_SIZE; i++){
            g_sizes[i] = 0 ;
            g_stacks_quantities[i] = 0 ;
        }
        g_stacks_size = 0 ;
    #ifdef USE_MPI
    }
    #endif
}


void end_size_profiling(){
    #ifdef USE_MPI
    g_mpi_enabled = 0 ;
    #endif
    g_malloc_hook_active = 0 ;
    g_realloc_hook_active = 0 ;
    g_calloc_hook_active = 0 ;
    for (int i = 0; i < g_stacks_size; i++){
        free(g_stacks[i]);
        free(g_symbolic_stacks[i]);
    }
    free(g_stacks);
    free(g_symbolic_stacks);

}


void dump_size_profiling(const char * a_file_prefix, const char * a_mode){
    #ifdef USE_MPI
    if(g_mpi_rank == 0){
    #endif
        char size_filename[2000];
        sprintf(size_filename, "%s.size", a_file_prefix);
        FILE * size_file = fopen(size_filename, a_mode);
        if (size_file == NULL){
            fprintf(stderr, "[%s] Couldn't open file '%s' in %s mode\n", __FUNCTION__, size_filename, a_mode); fflush(stderr);
            exit(EXIT_FAILURE);
        }
        fprintf(stderr, "Dumping size profile into %s\n", size_filename);
        print_size_profiling(size_file);
        fclose(size_file);

        char stack_filename[2000];
        sprintf(stack_filename, "%s.stack", a_file_prefix);
        FILE * stack_file = fopen(stack_filename, a_mode);
        if (stack_file == NULL){
            fprintf(stderr, "[%s] Couldn't open file '%s' in %s mode\n", __FUNCTION__, stack_filename, a_mode); fflush(stderr);
            exit(EXIT_FAILURE);
        }
        fprintf(stderr, "Dumping size profile into %s\n", stack_filename);
        print_global_stack(stack_file);
        fclose(stack_file);
    #ifdef USE_MPI
    }
    #endif
}


void add_allocation(size_t size){
    switch(size) {
        // Small object case
        case 0 ... SMALL_OBJECT_SIZE  :
            #ifdef EXACT_SIZE
            g_sizes[size]++; // Record the exact size of small objects
            #endif
            g_total_quantity_small += size ;
            g_sizes[SMALL_OBJECT_INDEX]++;
            break;

        // Medium object case
        case SMALL_OBJECT_SIZE + 1 ... MEDIUM_OBJECT_SIZE  :
            g_total_quantity_medium += size ;
            g_sizes[MEDIUM_OBJECT_INDEX]++;
            break;

        // Large object case
        default :
            g_total_quantity_large += size ;
            g_sizes[LARGE_OBJECT_INDEX]++;
            break;
    }
}


// ########################### Malloc redefinition ############################

// malloc hooking adapted from https://stackoverflow.com/a/17850402/6304206

void* my_malloc_hook (size_t size){
    void *ptr;

    g_malloc_hook_active = 0;

    ptr = malloc(size);
    if(needs_to_be_recorded(size)){
        #pragma omp critical
        {
        int is_new = add_stack();
        if(is_new){
            record_symbol_stack();
        }
        }
    }
    add_allocation(size);

    g_malloc_hook_active = 1;

    return ptr;
}


void* malloc (size_t size) throw (){
    #ifdef USE_MPI
    if(g_mpi_enabled){
        MPI_Comm_rank(MPI_COMM_WORLD, &g_mpi_rank);
        if (g_malloc_hook_active && g_mpi_rank == 0){
            return my_malloc_hook(size);
        }
        return __libc_malloc(size);
    }
    else{
    #endif
        if (g_malloc_hook_active){
            return my_malloc_hook(size);
        }
        return __libc_malloc(size);
    #ifdef USE_MPI
    }
    #endif
}


void* my_realloc_hook (void* ptr, size_t size){
    void *res;

    g_realloc_hook_active = 0;

    res = realloc(ptr, size);
    if(needs_to_be_recorded(size)){
        #pragma omp critical
        {
        int is_new = add_stack();
        if(is_new){
            record_symbol_stack();
        }
        }
    }
    add_allocation(size);

    g_realloc_hook_active = 1;

    return res;
}


void* realloc (void* ptr, size_t size) throw (){
    #ifdef USE_MPI
    if(g_mpi_enabled){
        MPI_Comm_rank(MPI_COMM_WORLD, &g_mpi_rank);
        if (g_realloc_hook_active && g_mpi_rank == 0){
            return my_realloc_hook(ptr, size);
        }
        return __libc_realloc(ptr, size);
    }
    else{
    #endif
        if (g_realloc_hook_active){
            return my_realloc_hook(ptr, size);
        }
        return __libc_realloc(ptr, size);
    #ifdef USE_MPI
    }
    #endif
}


void* my_calloc_hook (size_t nmemb, size_t size){
    void *ptr;

    g_calloc_hook_active = 0;

    ptr = calloc(nmemb, size);
    if(needs_to_be_recorded(size)){
        #pragma omp critical
        {
        int is_new = add_stack();
        if(is_new){
            record_symbol_stack();
        }
        }
    }
    add_allocation(size);

    g_calloc_hook_active = 1;

    return ptr;
}


void* calloc (size_t nmemb, size_t size) throw (){
    #ifdef USE_MPI
    if(g_mpi_enabled){
        MPI_Comm_rank(MPI_COMM_WORLD, &g_mpi_rank);
        if (g_calloc_hook_active && g_mpi_rank == 0){
            return my_calloc_hook(nmemb, size);
        }
        return __libc_calloc(nmemb, size);
    }
    else{
    #endif
        if (g_calloc_hook_active){
            return my_calloc_hook(nmemb, size);
        }
        return __libc_calloc(nmemb, size);
    #ifdef USE_MPI
    }
    #endif
}
