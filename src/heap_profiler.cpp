#include "heap_profiler.h"

void init_heap_profiling (char const * prefix){
    HeapProfilerStart(prefix);
}

void end_heap_profiling (){
    HeapProfilerStop();
}

void heapProfilerDump(char const * prefix){
    HeapProfilerDump(prefix);
}