#include "cpu_profiler.h"

void init_cpu_profiling (char const * prefix){
  ProfilerStart (prefix) ;
}

void end_cpu_profiling (){
  ProfilerStop () ;
}